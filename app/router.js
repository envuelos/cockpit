import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('pilots', function(){
    this.route('new');
    this.route('show', { path: ':pilot_id' });
    this.route('edit', { path: ':pilot_id/edit'});
  });
  this.route('gliders', function() {
    this.route('new');
    this.route('show', { path: ':glider_id' });
    this.route('edit', { path: ':glider_id/edit' });
  });
  this.route('airplanes', function() {
    this.route('new');
    this.route('show', { path: ':airplane_id' });
    this.route('edit', { path: ':airplane_id/edit' });
  });

  this.route('dashboard');
  this.route('login');
});

export default Router;
