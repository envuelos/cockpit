import GlidersBaseController from './base';

export default GlidersBaseController.extend({
  actions: {
    cancel: function() {
      this.transitionToRoute('gliders.show', this.get('model'));
      return false;
    }
  }
});
