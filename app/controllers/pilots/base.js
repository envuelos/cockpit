import Ember from 'ember';

export default Ember.Controller.extend({
  isValid: Ember.computed(
    'model.firstName', 'model.lastName',
    function(){
      return !Ember.isEmpty(this.get('model.firstName')) &&
        !Ember.isEmpty(this.get('model.lastName'));
    }
  ),
  actions: {
    save: function() {
      if(this.get('isValid')){
        this.get('model').save().then( pilot => {
          this.transitionToRoute('pilots.show', pilot);
        });
      } else {
        this.set('errorMessage', 'You have to fill all the fields');
      }
      return false;
    },
    cancel: function() {
      return true;
    }
  }
});
