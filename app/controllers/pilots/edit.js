import PilotsBaseController from './base';

export default PilotsBaseController.extend({
  actions: {
    cancel: function() {
      this.transitionToRoute('pilots.show', this.get('model'));
      return false;
    }
  }
});
