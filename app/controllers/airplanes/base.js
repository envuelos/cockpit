import Ember from 'ember';

export default Ember.Controller.extend({
  isValid: Ember.computed(
    'model.brand', 'model.model', 'model.registration',
    function () {
      return !Ember.isEmpty(this.get('model.brand')) &&
        !Ember.isEmpty(this.get('model.model')) &&
        !Ember.isEmpty(this.get('model.registration'));
    }
  ),
  actions: {
    save: function() {
      if(this.get('isValid')){
        this.get('model').save().then( airplane => {
          this.transitionToRoute('airplanes.show', airplane);
        });
      } else {
        this.set('errorMessage', 'Please fill all fields before continuing');
      }
      return false;
    },
    cancel: function () {
      return true;
    }
  }
});
