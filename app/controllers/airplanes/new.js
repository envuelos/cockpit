import AirplanesBaseController from './base';

export default AirplanesBaseController.extend({
  actions: {
    cancel: function() {
      this.transitionToRoute('airplanes.index');
      return false;
    }
  }
});
