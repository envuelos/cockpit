import AirplanesBaseController from './base';

export default AirplanesBaseController.extend({
  actions: {
    cancel: function() {
      this.transitionToRoute('airplanes.show', this.get('model'));
      return false;
    }
  }
});
