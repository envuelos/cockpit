import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
  brand: DS.attr('string'),
  model: DS.attr('string'),
  registration: DS.attr('string'),
  prettyName: Ember.computed('brand', 'model', 'registration', function(){
    return `${this.get('brand')} ${this.get('model')} - ${this.get('registration')}`;
  })
});
