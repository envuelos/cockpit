import DeviseAuthenticator from 'ember-simple-auth/authenticators/devise';

export default DeviseAuthenticator.extend({
  resourceName: 'account',
  serverTokenEndpoint: '/accounts/sign_in'
});